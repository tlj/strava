#!/usr/bin/env bash

rm -rf strava_api/*
/opt/homebrew/Cellar/openjdk/18.0.1/bin/java -jar ~/swagger-codegen.jar generate \
  -i swagger/swagger.json -l go \
  -o strava_api --additional-properties \
  packageName=strava_api
# Strava API (Go)

Package with generated Strava API swagger code, with some modifications.

## Usage

```go
package main

import "gitlab.com/tlj/strava/strava_api"

func main() {
  c := strava_api.NewAPIClient(strava_api.NewConfiguration())
  ...
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)